import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSkipComponent } from './report-skip.component';

describe('ReportSkipComponent', () => {
  let component: ReportSkipComponent;
  let fixture: ComponentFixture<ReportSkipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSkipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSkipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
