import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-report-activity',
  templateUrl: './report-activity.component.html',
  styleUrls: ['./report-activity.component.css']
})
export class ReportActivityComponent implements OnInit {

  reports: any[];
  form;

  constructor(
    private http: HttpClient,
    
  ) {

  }

  ngOnInit() {
    this.getReports()
  }
  getReports(){
    this.http.get('http://localhost:3000/api/reports')
      .subscribe((response: any) => {
        this.reports = response.data;
        console.log(this.reports);
      })
  }

}
