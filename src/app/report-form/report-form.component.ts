import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-report-form',
  templateUrl: './report-form.component.html',
  styleUrls: ['./report-form.component.css']
})
export class ReportFormComponent implements OnInit {
  
  projects: any[];
  form;

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      date: '',
      id_user: 1,
      id_project: 0,
      comment: '',
      duration: 0,
      state: 1
    });
  }

  ngOnInit() {
    this.getAllProjects();
  }

  getAllProjects() {
    this.http.get('http://localhost:3000/api/projects')
      .subscribe((response: any) => {
        this.projects = response.data;
      })
  }

  onSubmit(customerData) {
    console.log(customerData);
    this.http.post('http://localhost:3000/api/report', customerData)
      .subscribe((response: any) => {
        console.log(response)
      })
  }

}
