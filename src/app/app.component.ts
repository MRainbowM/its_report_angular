import { UsersService } from './services/users.service';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'report';
  userId: number = 0;
  response: any;

  constructor(private http: HttpClient){ }
 
  getUser(){
    this.http.get('http://localhost:3000/api/user/' + this.userId)
      .subscribe(response =>{
        this.response = response;
        console.log(this.response);
      })
  }
  
 
  
}


